/* mae.earth/toys/queue.go 
 * mae 12107
 */
package mqeueue

import (
	"sync"
	"errors"
)

var (
	ErrNotConfigured = errors.New("Not Configured")
	ErrNotBegun = errors.New("Not Begun")
	ErrAlreadyBegun = errors.New("Already Begun")
)



/* Queue */
type Queue struct {

	/* TODO */

	/* Queues should hold an ABAC policy */

}



/* Configuration */
type Configuration struct {


}


/* internal */
var internal struct {
	sync.RWMutex


	started bool
}




/* Configure */
func Configure(conf *Configuration) error {
	internal.Lock()
	defer internal.Unlock()

	if internal.started {
		return ErrAlreadyBegun
	} 


	return nil
}

/* Begin */
func Begin() error {
	internal.Lock()
	defer internal.Unlock()

	if internal.started {
		return ErrAlreadyBegun
	}

	return nil
}

/* End */
func End() error {
	internal.Lock()
	defer internal.Unlock()

	if ! internal.started {
		return ErrNotBegun
	}

	return nil
}

/* Options user options */
type Options struct {

	CopyOnly bool /* do NOT actually dequeue the message, but just copy */
}


/* Enqueue */
func Enqueue(content []byte,options Options) (string,error) {
	internal.RLock()
	defer internal.RUnlock()

	if ! internal.started {
		return "",ErrNotBegun
	}


	

	return "msg-id",nil
}

/* BatchEnqueue TODO: improve */
func BatchEnqueue(content []byte,options Options) ([]string,error) {
	internal.RLock()
	defer internal.RUnlock()

	if !internal.started {
		return nil,ErrNotBegun
	}


	return []string{"msg-id"},nil
}


/* View TODO: needs a better NAME, also needs to return a structure and a cursor based system for looking up message ids, totals etc */
func View(q string,options Options) ([]string,error) {
	internal.RLock()
	defer internal.RUnlock()

	if ! internal.started {
		return nil,ErrNotBegun
	}

	return []string{"msg-id"},nil
}

/* TODO: to note the singular api functions are not really needed as the batch versions could easily take the singular items */

/* Dequeue */
func Dequeue(msgid string,options Options) ([]byte,error) { 
	internal.RLock()
	defer internal.RUnlock()

	if ! internal.started {
		return nil,ErrNotBegun
	}

	return []byte("mesg"),nil
}

/* BatchDequeue TODO: improve */
func BatchDequeue(msgids []string,options Options) ([]byte,error) {
	internal.RLock()
	defer internal.RUnlock()

	if ! internal.started {
		return nil,ErrNotBegun
	}

	return []byte("msg"),nil
}








