/* mae.earth/toys/rbac/rbac_test.go */
package rbac

import (
	. "github.com/smartystreets/goconvey/convey"
	"testing"
	"time"
	"strings"
)

func Test_RBAC(t *testing.T) {

	Convey("RBAC",t,func() {

		Convey("create policy",func() {

			p := Policy{Name:"test",Created:time.Now(),Rules:[]Ruler{ MustMatch{role:Role("admin"),object:"foo=bar",set:Set{r:true,w:true}} } }
			So(p.Name,ShouldEqual,"test")		
			So(p.Created.IsZero(),ShouldBeFalse)
			So(len(p.Rules),ShouldEqual,1)
		})

		Convey("grant builder",func() {

			/* set some roles */
			admin := Role("admin")
			object := Object(make(map[string]string,0))

			gb := &GrantBuilder{}
			So(gb.Validate(),ShouldEqual,ErrInvalidGrantBuilder)

			gb.WithRole(admin)
			So(gb.Validate(),ShouldEqual,ErrInvalidGrantBuilder)

			gb.WithObject(object)
			So(gb.Validate(),ShouldEqual,ErrInvalidGrantBuilder)

			gb.WithEnvironment()
			So(gb.Validate(),ShouldEqual,ErrInvalidGrantBuilder)

			gb.WithAction(WRITE)
			So(gb.Validate(),ShouldEqual,nil)

		})

		Convey("Check that object has id",func() {

			object := Object(make(map[string]string,0))
			object.SetId("this-is-an-id")
			
			So(object.Id(),ShouldEqual,"this-is-an-id")
		})

		Convey("Simple grant",func() {
		
			admin := Role("admin")
			user := Role("user")
			object := Object(make(map[string]string,0))
			object.SetAttribute("foo","bar","sid","man")

			So(strings.Join(object.GetAttribute("foo"),";"),ShouldEqual,"bar;sid;man")

			policy := Policy{Name:"test",Created:time.Now(),Rules:[]Ruler{ MustMatch{role:admin,object:"foo=bar",set:Set{r:true,w:true}} } }

			gb := new(GrantBuilder)
			
			So(policy.Grant(gb.WithRole(admin).WithObject(object).WithEnvironment().WithAction(WRITE)),ShouldBeNil)
			So(gb.HaveGrant(),ShouldBeTrue)
			So(gb.Audit(),ShouldEqual,"role=admin objectid=unknown action=write grant=yes 2e7d628b")

			So(policy.Grant(gb),ShouldEqual,ErrGrantBuilderAlreadyRun)

			gb = new(GrantBuilder)

			So(policy.Grant(gb.WithRole(user).WithObject(object).WithEnvironment().WithAction(WRITE)),ShouldBeNil)
			So(gb.HaveGrant(),ShouldBeFalse)
			So(gb.Audit(),ShouldEqual,"role=user objectid=unknown action=write grant=no 4ed57795")
		})

	})
}
