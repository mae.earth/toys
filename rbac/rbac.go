/* mae.earth/toys/rbac/rbac.go */
package rbac

import (
	"strings"
	"time"
	"errors"
	"fmt"
	
	"crypto/sha1"
	"encoding/hex"
)

/* s/expr :-
 *
 * (POLICY "test" (
 *   (MUSTMATCH (ROLE "admin") (OBJECT "foo=bar") (SET (WRITE READ)))))
 *
 
POLICY "test"
	MUSTMATCH 
		ROLE "admin"
		OBJECT "foo=bar"
		SET 
			WRITE
			READ

*/


var (
	ErrInvalidGrantBuilder = errors.New("invalid grant builder")
	ErrGrantBuilderAlreadyRun = errors.New("already run grant builder")
)

const Delimiter string = ";"

/* Action */
type Action string

const (
	READ = Action("read")
	WRITE = Action("write")
	CREATE = Action("create")
	DELETE = Action("delete")
	COPY = Action("copy")
	RENAME = Action("rename")
	EXECUTE = Action("execute")
)


/* Set */
type Set struct {
	r bool /* read */
	w bool /* write */
	c bool /* create */
	d bool /* delete */
	cp bool /* copy */
	rn bool /* rename */
	x bool /* execute */
}

/* Read */
func (s Set) Read() bool { return s.r }
/* Write */
func (s Set) Write() bool { return s.w }
/* Create */
func (s Set) Create() bool { return s.c }
/* Delete */
func (s Set) Delete() bool { return s.d }
/* Copy */
func (s Set) Copy() bool { return s.cp }
/* Rename */
func (s Set) Rename() bool { return s.rn }
/* Execute */
func (s Set) Execute() bool { return s.x }

var FalseSet = Set{r:false,w:false,c:false,d:false,cp:false,rn:false,x:false}


/* Role of the "subject" */
type Role string

/* ObjectAttributer */
type ObjectAttributer interface {
	SetAttribute(key string,values ...string)
	GetAttribute(key string) []string
	Id() string
}


/* Object : default attributer */
type Object map[string]string

/* SetAttribute */
func (object Object) SetAttribute(key string,values ...string) {

	if key == "" {
		return 
	}
	
	if len(values) > 0 {
		object[ key ] = strings.Join(values,Delimiter)	
	}
}

/* GetAttribute */
func (object Object) GetAttribute(key string) []string {

	v,ok := object[ key ]
	if !ok {
		return nil
	}

	return strings.Split(v,Delimiter)
}

func (object Object) Id() string {
	if v,ok := object["id"]; ok {
		return v
	}
	return "unknown"
}	

func (object Object) SetId(id string) {
	object[ "id" ] = id
}




/* Environment */
type Environment struct {
	Time time.Time
}

/* Ruler */
type Ruler interface {
	Eval(Role,ObjectAttributer,Environment) Set
}

type MustMatch struct {
	role Role
	object string /* key=value */
	set Set
}

func (m MustMatch) Eval(role Role,object ObjectAttributer,env Environment) Set {

	if role != m.role || object == nil {
		return FalseSet
	}

	parts := strings.Split(m.object,"=")
	if len(parts) != 2 {
		return FalseSet
	}

	for _,value := range object.GetAttribute(parts[0]) {
		if value == parts[1] {
			return m.set
		}
	}

	return FalseSet
}



/* Policy */
type Policy struct {
	Name string
	Created time.Time

	Rules []Ruler
}

func (p Policy) Grant(builder *GrantBuilder) error {
	if builder == nil {
		return ErrInvalidGrantBuilder
	}
	if err := builder.Validate(); err != nil {
		return err
	}
	
	if builder.audit != "" {
		return ErrGrantBuilderAlreadyRun
	}

	s := FalseSet

	for _,rule := range p.Rules {
		rs := rule.Eval(builder.role,builder.object,builder.environ)
		s.r = rs.r
		s.w = rs.w
		s.c = rs.c
		s.d = rs.d
		s.cp = rs.cp
		s.rn = rs.rn
		s.x = rs.x
	}

	ans := false

	switch builder.action {
		case READ:
			ans = s.r
		break
		case WRITE:
			ans = s.w
		break
		case CREATE:
			ans = s.c
		break
		case DELETE:
			ans = s.d
		break
		case COPY:
			ans = s.cp
		break
		case RENAME:
			ans = s.rn
		break
		case EXECUTE:
			ans = s.x
		break
	}

	builder.answer = ans
	tag := "no"
	if ans {
		tag = "yes"
	}
	

	audit := fmt.Sprintf("role=%s objectid=%s action=%s grant=%s",string(builder.role),builder.object.Id(),string(builder.action),tag)
	
	sha := sha1.New()
	sha.Write([]byte(audit))
	builder.audit = audit + " " + hex.EncodeToString(sha.Sum(nil))[:8]

	return nil
} 

/* GrantBuilder */
type GrantBuilder struct {
	role Role
	object ObjectAttributer
	environ Environment
	action Action

	answer bool
	audit string
}

/* WithRole */
func (builder *GrantBuilder) WithRole(role Role) *GrantBuilder {
	builder.role = role
	return builder
}

/* WithObject */
func (builder *GrantBuilder) WithObject(object ObjectAttributer) *GrantBuilder {
	builder.object = object 
	return builder
}

/* WithAction */
func (builder *GrantBuilder) WithAction(action Action) *GrantBuilder {
	builder.action = action
	return builder
}

/* WithEnvironment */
func (builder *GrantBuilder) WithEnvironment() *GrantBuilder {
	builder.environ = Environment{Time:time.Now()}
	return builder
}

/* Validate */
func (builder *GrantBuilder) Validate() error {
	if string(builder.role) == "" {
		return ErrInvalidGrantBuilder
	}
	if builder.object == nil {
		return ErrInvalidGrantBuilder
	}
	if builder.environ.Time.IsZero() {
		return ErrInvalidGrantBuilder
	}
	if string(builder.action) == "" {
		return ErrInvalidGrantBuilder
	}

	return nil
}

/* HaveGrant */
func (builder *GrantBuilder) HaveGrant() bool {
	return (builder.answer == true)
}

/* Audit */
func (builder *GrantBuilder) Audit() string {
	return builder.audit
}










