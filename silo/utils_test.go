/* mae.earth/toys/silo/utils_test.go
 * mae 12017
 */
package main

import (
	. "github.com/smartystreets/goconvey/convey"
	"testing"
	"fmt"
)

func Test_Utils(t *testing.T) {

	Convey("Utilities",t,func() {

		Convey("ipsomTitle",func() {

			for i := 0; i < 10; i++ {
				title := ipsomTitle()
				So(title,ShouldNotBeEmpty)
				So(len(title),ShouldBeLessThan,256 + 10)
				fmt.Printf("%02d ipsomTitle = \"%s\":%d\n",i + 1,string(title),len(title))
			}
		})

		Convey("ipsomText",func() {

			for i := 0; i < 3; i++ {
				text := ipsomText()
				So(text,ShouldNotBeEmpty)
				fmt.Printf("======= %d/3\n%s\n\nlength = %dbytes\n\n",i + 1,string(text),len(text))
			}
		})

		Convey("ipsomArticle",func() {

			title,body := ipsomArticle()
			So(title,ShouldNotBeEmpty)
			So(body,ShouldNotBeEmpty)
			fmt.Printf("Article\n\n%s\n\n%s\n",string(title),string(body))
		})
				
		
	})
}  



