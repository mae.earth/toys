/* mae.earth/toys/silo/sleeve.go
 * mae 12017
 */
package main

import (
	"time"
	"bytes"
	"errors"
	"crypto/sha1"
	"encoding/hex"
	"encoding/binary"

	"github.com/vmihailenco/msgpack"
)

const (
	Magic = byte('S')
	Version = byte(0)

	ChecksumLength int = 8
)

var (
	ErrBadMagic = errors.New("bad magic")
	ErrBadVersion = errors.New("bad version")
	ErrBadChecksum = errors.New("bad checksum")
)



type Sleeve struct {
	
	Created time.Time						`msgpack:"c"`
	Modified time.Time					`msgpack:"m"`

	ContentType string 					`msgpack:"ct"`
	ContentDisposition string		`msgpack:"cd"`

	Version int									`msgpack:"v"`
	Checksum string							`msgpack:"cs"`

	Content []byte							`msgpack:"-"`
}

/* Encode */
func (sleeve *Sleeve) Encode() ([]byte,error) {

	msgcontent,err := msgpack.Marshal(sleeve)
	if err != nil {
		return nil,err
	}

	l := uint64(len(msgcontent))
	
	buf := bytes.NewBuffer(nil)
	buf.WriteByte(Magic)
	buf.WriteByte(Version)

	if err := binary.Write(buf,binary.LittleEndian,l); err != nil {
		return nil,err
	}
		
	buf.Write(msgcontent)

	buf.Write(sleeve.Content)

	return buf.Bytes(),nil
}


/* ToSleeve */
func ToSleeve(content []byte) (*Sleeve,error) {

	var l uint64
	buf := bytes.NewBuffer(content)
	magic,err := buf.ReadByte()
	if err != nil {
		return nil,ErrBadMagic
	}	

	if magic != Magic {
		return nil,ErrBadMagic
	}

	version,err := buf.ReadByte()
	if err != nil {
		return nil,ErrBadVersion
	}

	if int(version) > int(Version) {
		return nil,ErrBadVersion
	}

	if err := binary.Read(buf,binary.LittleEndian,&l); err != nil {
		return nil,err
	}

	var sleeve *Sleeve
	if err := msgpack.Unmarshal(buf.Next(int(l)),&sleeve); err != nil {		
		return nil,err
	}

	con := buf.Bytes()
	if len(con) > 0 {
		sleeve.Content = make([]byte,len(con))
		copy(sleeve.Content,con)

		/* do a checksum check */
		sha := sha1.New()
		sha.Write(sleeve.Content)
		if hex.EncodeToString(sha.Sum(nil))[:ChecksumLength] != sleeve.Checksum {
			return sleeve,ErrBadChecksum
		}
	}	

	return sleeve,nil
}

/* GetContent */
func GetContent(content []byte) ([]byte,error) {

	var l uint64
	buf := bytes.NewBuffer(content)
	magic,err := buf.ReadByte()
	if err != nil {
		return nil,ErrBadMagic
	}	

	if magic != Magic {
		return nil,ErrBadMagic
	}

	version,err := buf.ReadByte()
	if err != nil {
		return nil,ErrBadVersion
	}

	if int(version) > int(Version) {
		return nil,ErrBadVersion
	}

	if err := binary.Read(buf, binary.LittleEndian, &l); err != nil {
		return nil,err
	}

	/* seek forward */
	buf.Next(int(l))

	return buf.Bytes(),nil
}

/* Copy */
func (sleeve *Sleeve) Copy() *Sleeve {

	nsleeve := NewSleeve(sleeve.ContentType,sleeve.ContentDisposition,sleeve.Content)
	nsleeve.Version = sleeve.Version + 1
	return nsleeve
}

/* CopyWithContent */
func (sleeve *Sleeve) CopyWithContent(content []byte) *Sleeve {

	nsleeve := NewSleeve(sleeve.ContentType,sleeve.ContentDisposition,content)
	nsleeve.Created = sleeve.Created
	nsleeve.Version = sleeve.Version + 1

	return nsleeve
}

/* NewSleeve */
func NewSleeve(ctype,cdispo string,content []byte) *Sleeve {

	sleeve := &Sleeve{Created:time.Now(),Modified:time.Now(),Version:0,ContentType:ctype,ContentDisposition:cdispo}
	
	if len(content) > 0 {
		sleeve.Content = make([]byte,len(content))
		copy(sleeve.Content,content)
	}

	sha := sha1.New()
	sha.Write(content)
	sleeve.Checksum = hex.EncodeToString(sha.Sum(nil))[:ChecksumLength]

	return sleeve
}
	
