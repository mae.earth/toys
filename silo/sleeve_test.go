/* mae.earth/toys/silo/sleeve_test.go
 * mae 12017
 */
package main

import (
	. "github.com/smartystreets/goconvey/convey"
	"testing"
	"time"
)

func Test_Sleeve(t *testing.T) {

	Convey("Sleeve",t,func() {

		Convey("basic usage",func() {

			now := time.Now()

			sleeve := NewSleeve("text/plain","text",[]byte("helo"))
			So(sleeve,ShouldNotBeNil)
			So(sleeve.ContentType,ShouldEqual,"text/plain")
			So(sleeve.ContentDisposition,ShouldEqual,"text")
			So(string(sleeve.Content),ShouldEqual,"helo")
			So(sleeve.Version,ShouldEqual,0)
			So(now.Before(sleeve.Created),ShouldBeTrue)
			So(now.Before(sleeve.Modified),ShouldBeTrue)

			Convey("encode",func() {

				content,err := sleeve.Encode()
				So(err,ShouldBeNil)
				So(content,ShouldNotBeEmpty)
				So(len(content),ShouldEqual,76)

				Convey("get content",func() {

					con,err := GetContent(content)
					So(err,ShouldBeNil)
					So(con,ShouldNotBeEmpty)
					So(string(con),ShouldEqual,"helo")
				})

				Convey("decode",func() {

					sleeve1,err := ToSleeve(content)
					So(err,ShouldBeNil)
					So(sleeve1,ShouldNotBeNil)
					So(sleeve1,ShouldNotBeNil)
					So(sleeve1.ContentType,ShouldEqual,"text/plain")
					So(sleeve1.ContentDisposition,ShouldEqual,"text")
					So(string(sleeve1.Content),ShouldEqual,"helo")
					So(sleeve1.Version,ShouldEqual,0)
					So(now.Before(sleeve1.Created),ShouldBeTrue)
					So(now.Before(sleeve1.Modified),ShouldBeTrue)
				})					
			})
		})
	})
}
	
		
func Benchmark_SleeveEncode(b *testing.B) {

	b.StopTimer()

	sleeve := NewSleeve("text/plain","text",[]byte(`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non convallis dolor, eu vehicula magna. Vivamus dapibus placerat arcu at dapibus. Aenean metus quam, eleifend vestibulum accumsan et, tristique non orci. Vivamus erat libero, eleifend vel condimentum et, pretium sit amet justo. Maecenas in pharetra ante, in suscipit sapien. Quisque interdum, ante vel aliquam congue, nibh felis lobortis dui, non convallis purus neque vitae felis. Curabitur imperdiet porta leo, at bibendum arcu pellentesque sed. Duis eu suscipit metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vehicula venenatis ligula, eu blandit ante iaculis et. In ultrices neque faucibus imperdiet porta. Nam venenatis est magna, id aliquam leo eleifend in.`))

	b.StartTimer()	

	for i := 0; i < b.N; i++ {

		sleeve.Encode()
	}
}


func Benchmark_SleeveGetContent(b *testing.B) {

	b.StopTimer()

	sleeve := NewSleeve("text/plain","text",[]byte(`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non convallis dolor, eu vehicula magna. Vivamus dapibus placerat arcu at dapibus. Aenean metus quam, eleifend vestibulum accumsan et, tristique non orci. Vivamus erat libero, eleifend vel condimentum et, pretium sit amet justo. Maecenas in pharetra ante, in suscipit sapien. Quisque interdum, ante vel aliquam congue, nibh felis lobortis dui, non convallis purus neque vitae felis. Curabitur imperdiet porta leo, at bibendum arcu pellentesque sed. Duis eu suscipit metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vehicula venenatis ligula, eu blandit ante iaculis et. In ultrices neque faucibus imperdiet porta. Nam venenatis est magna, id aliquam leo eleifend in.`))

	content,err := sleeve.Encode()
	if err != nil {
		b.Fatal(err)
	}

	b.StartTimer()	

	for i := 0; i < b.N; i++ {

		GetContent(content)		
	}
}


func Benchmark_SleeveDecode(b *testing.B) {

	b.StopTimer()

	sleeve := NewSleeve("text/plain","text",[]byte(`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non convallis dolor, eu vehicula magna. Vivamus dapibus placerat arcu at dapibus. Aenean metus quam, eleifend vestibulum accumsan et, tristique non orci. Vivamus erat libero, eleifend vel condimentum et, pretium sit amet justo. Maecenas in pharetra ante, in suscipit sapien. Quisque interdum, ante vel aliquam congue, nibh felis lobortis dui, non convallis purus neque vitae felis. Curabitur imperdiet porta leo, at bibendum arcu pellentesque sed. Duis eu suscipit metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vehicula venenatis ligula, eu blandit ante iaculis et. In ultrices neque faucibus imperdiet porta. Nam venenatis est magna, id aliquam leo eleifend in.`))

	content,err := sleeve.Encode()
	if err != nil {
		b.Fatal(err)
	}

	b.StartTimer()	

	for i := 0; i < b.N; i++ {

		ToSleeve(content)		
	}
}


