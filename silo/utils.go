/* mae.earth/toys/silo/utils.go
 * mae 12017
 */
package main

import (
	"crypto/rand"
	"strings"
	"bytes"
	"encoding/hex"
)

func uuid() string {
	b := make([]byte,10)
	rand.Read(b)
	return hex.EncodeToString(b)
}

func Number(max int) int {

	b := make([]byte,2)
	rand.Read(b)

	n := int(b[0]) * int(b[1])
	if n > max {
		n = max
	}

	return n
}


func ipsomTitles(max int) [][]byte {

	b := make([]byte,1)
	rand.Read(b)

	n := int(b[0]) + 1
	if n > max {
		n = max
	}

	out := make([][]byte,0)

	for i := 0; i < n; i++ {
		out = append(out,[]byte(strings.Replace(string(ipsomTitle())," ","",-1)))
	}
	return out
}
	

func ipsomArticle() ([]byte,[]byte) {

	title := ipsomTitle()
	body := ipsomText()

	buf := bytes.NewBuffer(nil)
	buf.Write(title)
	buf.Write([]byte("\n==========================================================================================================\n\n"))
	buf.Write(body)

	return title,buf.Bytes()
}
	

func ipsomText() []byte {

	b := make([]byte,4)
	rand.Read(b)

	i := int(b[0]) * int(b[1]) / 3
	l := int(b[2]) * int(b[3]) / 2

	if l < 256 {
		l = 256
	}

	if i + l > len(ipsom) {
		l = len(ipsom) - (i + 1)
	}


	return []byte(strings.TrimSpace(ipsom[i:i + l]))
}

func ipsomTitle() []byte {

	b := make([]byte,3)
	rand.Read(b)

	i := int(b[0]) *  int(b[1]) / 2 /* max 256 * 128 = 32768 */
	l := int(b[2]) 
	if l > 64 {
		l = 64
	}
	if l < 12 {
		l = 12
	}

	return []byte(strings.TrimSpace(strings.Replace(ipsom[i:i + l],"\n","",-1)))
}

const ipsom = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec laoreet odio sed posuere tempor. Proin faucibus lacinia velit at dictum. Aenean molestie ac tortor non tempor. Sed molestie mi a mattis euismod. Nulla iaculis turpis mauris, ut scelerisque dui bibendum sed. Integer nec risus vitae odio eleifend fermentum et et quam. Etiam leo ante, ultrices eget accumsan aliquet, scelerisque et urna. Sed a nisl eget nisl rhoncus tempor vel quis erat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In vitae ipsum et libero venenatis consequat vitae a dolor. Curabitur cursus urna ex, eu placerat mi dictum vitae. Quisque condimentum ultrices nisi, sed elementum ligula. Morbi in blandit tortor. Aliquam fringilla lobortis arcu vel pharetra.

Sed a maximus nisi, sed malesuada nisi. Donec vitae mattis turpis, eget fringilla dui. Mauris sagittis, nibh eget lacinia ultricies, nulla quam lobortis diam, eget volutpat tortor urna a ex. Donec vestibulum dictum nunc a convallis. In tincidunt ornare fringilla. Nulla ac ante sit amet nulla suscipit pretium. Pellentesque odio lectus, consequat sed nisi ac, vulputate gravida elit. Sed in metus at erat laoreet pretium ut at est.

Donec lectus turpis, eleifend eu molestie id, porttitor sed ipsum. In hac habitasse platea dictumst. Aenean feugiat lorem ut tortor fermentum, in vulputate nisi posuere. Curabitur bibendum elementum eros sit amet sollicitudin. Mauris venenatis, leo id luctus viverra, odio nibh fermentum nibh, nec vulputate quam nulla non eros. Nunc ullamcorper vehicula lectus, eu ultricies velit consequat vel. Phasellus vel dui consectetur odio faucibus bibendum. Suspendisse sodales fermentum est, at dignissim justo fringilla et. Nulla porttitor lectus et eros viverra, accumsan consectetur est pellentesque. Sed sollicitudin urna diam, vel luctus lacus tristique ac. Donec a porta sem, ac tempus nibh. In a odio tincidunt lacus porta mattis. Aliquam venenatis nisi at justo ornare fermentum. Nulla sit amet tortor ligula. Sed sed eros arcu. Pellentesque venenatis ante in consequat scelerisque.

Mauris dapibus varius nisl, quis blandit neque venenatis in. Phasellus aliquam, arcu in rhoncus blandit, augue arcu placerat nunc, a laoreet ipsum tortor eget magna. In varius, nisl eu molestie accumsan, lorem est porttitor eros, in bibendum risus quam nec leo. Quisque dapibus porttitor velit euismod tempor. Morbi maximus sapien quis arcu ullamcorper ornare. In eu velit quis massa vulputate convallis id semper felis. Morbi scelerisque enim eros, sit amet luctus risus molestie sit amet. Ut accumsan mollis massa, vitae mattis tellus volutpat sed.

Aenean et odio tempor, auctor mauris sit amet, facilisis leo. In finibus non enim eu malesuada. Integer suscipit nibh id finibus bibendum. Integer vehicula, nunc nec tristique rhoncus, ligula lectus posuere velit, eu tempus eros elit non leo. Morbi ut tristique dui, id sollicitudin risus. Curabitur sit amet elit varius erat porttitor volutpat sed nullam.

Maecenas nisi metus, malesuada ut imperdiet a, bibendum eu felis. Praesent sed magna ante. Aenean quis lacus et nisi malesuada luctus. Curabitur efficitur eros eget neque iaculis, eu porttitor ex pretium. Mauris nec massa ac sapien varius dapibus sed eu libero. Suspendisse bibendum a urna ut interdum. Vestibulum id lacus semper, semper ante vel, maximus tellus. Nam metus quam, pellentesque vel sodales quis, lobortis nec mi. Etiam eget purus nulla. Phasellus faucibus tellus in pellentesque blandit. Nam tincidunt sem non commodo tincidunt. Nulla euismod auctor enim, sit amet pretium elit scelerisque convallis. Vivamus rhoncus, elit non ullamcorper congue, mi felis elementum ligula, vel sagittis nunc neque et ipsum.

Integer vulputate tempus mollis. Cras ultricies feugiat odio, in fermentum velit interdum sit amet. Vestibulum condimentum varius metus, at aliquam lectus rhoncus vitae. Curabitur sodales varius libero eu porta. Fusce mattis sapien eu ipsum molestie, eu volutpat arcu faucibus. Fusce fermentum tempor sapien quis pellentesque. Duis ornare lacus eget lorem lobortis viverra. Curabitur luctus interdum laoreet. Morbi laoreet ut metus finibus tempor. Curabitur feugiat tristique augue ut faucibus. Cras varius ipsum nisi, et pellentesque nibh elementum at. In laoreet, lectus non semper dictum, nisi erat eleifend nisi, vitae gravida tellus sem sit amet urna. Ut odio tellus, imperdiet ut blandit a, laoreet finibus lectus. Donec tristique, eros pellentesque faucibus pulvinar, dui turpis maximus justo, vitae luctus velit risus in diam. Sed id vehicula nulla, vel efficitur elit.

Nam nulla lacus, iaculis quis malesuada a, cursus at dui. Nam convallis massa ac rutrum lobortis. Proin vel augue eu purus suscipit porttitor. Maecenas tellus ante, lacinia vitae faucibus quis, dictum ac elit. Aliquam blandit blandit velit, at rutrum leo rutrum quis. Nam egestas molestie risus id egestas. Donec non quam a diam varius faucibus. Mauris malesuada lobortis mollis. Proin a elit sit amet metus pretium luctus. In justo erat, tincidunt et ex eget, rhoncus maximus arcu. Maecenas fermentum neque quis sodales cursus.

Nunc ultricies, velit sed pellentesque finibus, tortor nunc pharetra mi, ac egestas nisl eros at eros. Morbi venenatis magna eget nibh suscipit mollis eu eu quam. Morbi nec augue vitae purus porta dignissim. Maecenas sed varius ante, ut cursus lacus. Integer libero lacus, facilisis id vulputate ut, sollicitudin eu lectus. Vestibulum pulvinar fringilla feugiat. Curabitur vestibulum condimentum quam vel ullamcorper. Pellentesque bibendum lacus sed fringilla pretium.

Mauris consectetur ante sed varius ultrices. Donec iaculis neque sit amet tempor accumsan. Nulla at tristique lorem, ac egestas ligula. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce non suscipit erat, eu feugiat nunc. Donec risus quam, tempus vel nunc id, posuere facilisis neque. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Aenean vitae eros sed nisl auctor congue ac id ante. Sed semper purus eros, eget tincidunt purus fringilla commodo. Duis velit mauris, laoreet eu iaculis ut, elementum sit amet massa. Curabitur in libero orci. Cras hendrerit leo nisi, nec bibendum tellus pulvinar non. Cras eget mi luctus, sagittis enim a, dignissim ante. Nam varius, lacus in scelerisque laoreet, ligula felis ultrices justo, vel commodo mauris est et ante. Morbi accumsan, elit id ultricies malesuada, lacus arcu facilisis leo, non dignissim nisi ligula in diam. Curabitur nec nibh pretium nulla dignissim volutpat non quis quam. Sed sed maximus ligula, in scelerisque turpis.

Cras et mauris erat. Phasellus rutrum erat id nulla fringilla hendrerit sit amet eget velit. Sed non rhoncus leo. Donec ornare quam mi, non iaculis mauris interdum id. Curabitur non justo quam. Pellentesque viverra dignissim eros, sit amet placerat purus euismod eget. Sed blandit nulla felis, sit amet viverra odio rutrum vel.

Proin at accumsan mauris, eu venenatis ipsum. Proin risus sapien, consectetur sed feugiat vel, pretium vel dolor. Sed hendrerit, metus a maximus aliquet, neque elit suscipit est, blandit sollicitudin purus est et nisi. Duis ut varius metus, eu pellentesque metus. Aliquam lobortis elit dui, in consequat mauris cursus vitae. Sed commodo augue vitae justo aliquam suscipit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Morbi at tortor auctor nisl commodo scelerisque. Sed maximus leo non tempor dictum. Sed et dolor vel ex viverra tempus. Donec egestas finibus accumsan. Fusce mattis ornare vulputate. Curabitur imperdiet pellentesque nibh vel facilisis. Morbi feugiat finibus dolor, sit amet interdum augue elementum vitae. Interdum et malesuada fames ac ante ipsum primis in faucibus.

Duis accumsan dapibus sagittis. Fusce ullamcorper, odio ut cursus porta, sapien diam pretium tortor, quis consectetur lorem ipsum a eros. Donec vel nisl sit amet tellus euismod fringilla ac id massa. Duis hendrerit lectus tempor urna euismod consectetur. Mauris congue massa ut dolor interdum, eget dictum augue finibus. Nullam euismod pellentesque diam, vel rutrum justo blandit vel. Mauris in odio nec risus rhoncus tempus. Curabitur dignissim leo vitae pharetra rhoncus. Morbi quis quam quis leo commodo viverra faucibus at turpis. Praesent ac sem a tellus laoreet interdum vitae ac tortor. Nunc rhoncus accumsan mauris eget vulputate. Nam egestas, leo sit amet varius hendrerit, justo tortor ultrices eros, in maximus lorem eros pharetra urna. Nulla varius lobortis imperdiet. Integer quis pulvinar tortor. In efficitur hendrerit nunc, at tincidunt ante facilisis sed.

Aenean dignissim a quam non efficitur. Curabitur iaculis, felis aliquam congue lobortis, purus purus bibendum lectus, bibendum faucibus justo nisl malesuada magna. Donec in tellus mattis, tempor est in, bibendum neque. Cras enim neque, auctor sed ultricies rhoncus, porttitor nec nisl. Pellentesque varius vulputate dolor, ac elementum dolor ultricies ut. Nam posuere euismod pellentesque. Ut et volutpat nisl. Cras felis mauris, venenatis eget sodales sed, tincidunt sit amet nisi. Duis hendrerit pretium lacus eget egestas. Pellentesque maximus condimentum felis ac auctor. Aenean eget lacinia erat. Suspendisse lorem nisl, posuere non ligula non, dignissim cursus orci. Curabitur ultricies consectetur nunc, sed dictum risus elementum et.

Aenean venenatis enim et lacus gravida, at auctor justo cursus. Aliquam eu libero posuere, lacinia lorem vitae, maximus dolor. Maecenas sed ipsum venenatis, ultrices augue vitae, interdum magna. Mauris nunc odio, fermentum non accumsan ullamcorper, tristique in massa. Nulla porttitor vel arcu sed dignissim. Praesent feugiat interdum vulputate. Aenean sit amet massa at odio dictum tempor. Pellentesque convallis lacus non lacus fringilla aliquet. Sed odio neque, cursus ut lectus sed, efficitur sodales tortor. Mauris dapibus tortor eget iaculis venenatis. Fusce ultrices hendrerit dictum.

Vestibulum et faucibus lacus. Suspendisse pharetra justo nec magna vehicula tincidunt. Etiam placerat elit et quam convallis congue. Nullam ornare lobortis quam, at vulputate mauris fringilla eu. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sit amet mauris ut est faucibus tempor et ut augue. Integer felis lacus, molestie sit amet sollicitudin id, blandit quis ante. Suspendisse potenti. Phasellus a dui ligula. Nam rutrum elit et quam dictum laoreet sed vitae ipsum.

Vestibulum orci lectus, molestie accumsan posuere eu, condimentum at nibh. Fusce gravida gravida nisl at porta. Donec sit amet lacus feugiat, lobortis ipsum at, vulputate ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi leo diam, tincidunt a turpis eget, dictum malesuada diam. Curabitur placerat tincidunt maximus. Fusce non diam non augue iaculis vulputate eu quis arcu.

Fusce elementum ac eros non vulputate. Nullam nec quam turpis. Donec sed semper orci, id laoreet elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin commodo interdum quam. Sed bibendum odio sit amet faucibus feugiat. Nam semper metus massa, sit amet aliquam lacus feugiat sed. Mauris fermentum velit ut enim aliquet, quis egestas orci auctor. Aliquam vel ante arcu. Cras euismod massa nulla, eget imperdiet eros faucibus eget. Suspendisse egestas, lacus sit amet maximus porttitor, sapien nisi placerat mi, eu volutpat nulla nisi vel nisi. Donec mauris nibh, ornare vel gravida eget, congue vitae lectus. Nullam scelerisque dui ac urna maximus faucibus sed sit amet urna.

Aliquam ullamcorper ultricies elit, a efficitur quam tincidunt quis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Integer auctor mauris sapien, ut lacinia ligula malesuada nec. Nunc rhoncus ut orci in hendrerit. Proin faucibus sodales tortor. Aliquam posuere molestie dolor a dictum. Nulla non elit id nibh elementum fermentum sit amet at lorem. Aenean tristique, sem quis dictum feugiat, velit erat imperdiet nibh, eu sagittis sem ipsum non enim. Nunc et justo ut sapien tristique dapibus. Donec sit amet fermentum est. Nullam molestie porta elementum. Sed lacinia sem et nunc semper, eu facilisis libero semper. Duis aliquam commodo tortor at dignissim. Maecenas sagittis est mauris, ac ultrices nibh pharetra et. Nulla facilisi.

Sed tincidunt arcu sed libero lobortis tristique. Praesent pharetra lectus quis lectus tempus, eget cursus sapien mattis. Nam ullamcorper, nulla eget pellentesque pharetra, urna tellus condimentum lectus, quis luctus risus ex eget dolor. Pellentesque condimentum tristique feugiat. Proin vel dui hendrerit, fermentum est dignissim, maximus tortor. Nulla facilisi. Nulla posuere, mauris quis tincidunt ornare, arcu diam aliquam dui, sit amet congue risus massa id neque.

Aliquam erat volutpat. Cras eget maximus mi, sed eleifend diam. Nulla facilisi. Sed commodo ex dolor. Curabitur sit amet diam ex. Maecenas egestas eget massa vel lacinia. Duis at tempor diam. In hac habitasse platea dictumst. Curabitur ornare purus augue. Quisque eget ipsum semper, posuere ipsum vitae, pharetra nunc. Nunc vitae tincidunt mauris. Mauris ut dictum mauris, sed hendrerit risus. Donec placerat, orci nec molestie ultrices, nulla elit laoreet nulla, eget eleifend ex dui a diam. Vivamus tristique eu nibh nec vestibulum. Nunc fermentum justo ac tincidunt accumsan. Nullam id tortor quis ante dictum viverra quis sit amet metus.

Duis varius quam tellus, eget mattis nibh sodales et. Sed vel diam lacus. Phasellus at nulla at turpis vestibulum mattis. Maecenas ultricies ultrices lacus, eget ullamcorper ante. Nullam imperdiet diam tortor, ut fringilla sapien semper id. Ut ut lacinia massa, a elementum enim. Nam tellus ipsum, vestibulum vel mollis quis, porttitor ut tortor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut aliquam orci id purus sagittis, accumsan finibus nulla faucibus. Vivamus ante leo, porta id commodo vel, aliquet ut nunc.

Integer molestie augue vel nulla tincidunt vestibulum. Pellentesque interdum purus vel porta maximus. Morbi ligula felis, tincidunt eget enim suscipit, vestibulum luctus massa. Nullam pretium mi at feugiat pretium. Vivamus venenatis non tortor et sollicitudin. Etiam mattis, velit a tincidunt porttitor, ligula lectus rutrum enim, et finibus justo eros eget metus. Nam vitae turpis dolor. Proin ac interdum neque, vitae congue massa. Quisque in ultrices diam, at lacinia dolor. Pellentesque mollis, lectus sed blandit venenatis, sem nisl aliquam tortor, sodales dignissim dui est nec arcu.

Phasellus aliquet, dui nec iaculis dictum, massa nibh facilisis eros, id accumsan felis ligula ac leo. Aenean dictum vehicula pulvinar. Aliquam varius, nunc nec lacinia condimentum, enim leo dapibus odio, eget interdum neque purus vitae lacus. Donec gravida, tortor vel posuere faucibus, purus lectus egestas mauris, id finibus erat nisl in ipsum. Aliquam erat volutpat. Maecenas vel massa a eros malesuada finibus. Quisque id est nec augue cursus malesuada. Maecenas posuere non sem et sollicitudin. Morbi dignissim sapien id accumsan semper. Aliquam maximus egestas tellus tincidunt pulvinar. Etiam facilisis ipsum non magna dictum sagittis. In nec orci dolor.

Pellentesque consectetur id nisi eu vehicula. Nullam sollicitudin libero augue, eget luctus purus posuere id. Nullam eget aliquam risus, ut sollicitudin tellus. Etiam velit magna, cursus quis augue vel, faucibus laoreet elit. Sed dapibus iaculis dignissim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sagittis sapien at lacus faucibus pellentesque. Nullam pretium vehicula nisl, sed convallis sem sollicitudin hendrerit. Suspendisse sed elit eu lorem euismod cursus. Donec malesuada gravida mauris vel egestas. Maecenas auctor eros non elit pretium ornare. Morbi fermentum, nisi vel aliquam volutpat, ipsum nisl aliquet erat, ut euismod lectus tortor eu nunc. Phasellus quis augue enim. Cras tincidunt eros quis tellus tempor, dictum gravida nisi mollis. Phasellus sagittis tincidunt velit, eu auctor ipsum blandit ac. Morbi pretium tortor quis sapien scelerisque, eget suscipit nulla feugiat.

Etiam vestibulum risus eget iaculis auctor. Donec viverra, magna sit amet elementum laoreet, justo dolor malesuada nisl, ac aliquam ante dui id nunc. Vivamus sed dui id augue fringilla efficitur. Mauris hendrerit fermentum ultricies. Nulla magna leo, semper quis mauris vel, viverra pretium mauris. Aliquam mattis tortor nulla, at mollis nulla rhoncus nec. Morbi tincidunt orci id leo mattis, vel faucibus est sodales. Vestibulum interdum, libero ac interdum vestibulum, dui nisl tempus quam, non sodales elit neque ut metus. Pellentesque eget faucibus nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut dignissim, velit at sollicitudin malesuada, purus lectus dapibus felis, eu euismod turpis nunc non lectus. Praesent quis odio sit amet felis ultrices vestibulum. Curabitur blandit mauris felis, vel dignissim tortor cursus id. Donec auctor mollis magna, ut luctus tortor auctor quis. Fusce quis erat vitae nulla efficitur pulvinar. Mauris commodo consequat risus sed imperdiet.

Aenean eget fringilla eros. Quisque ut mi mi. Duis imperdiet, magna quis malesuada vehicula, dui urna dapibus sapien, vitae mollis augue erat ac lectus. Donec ultrices metus et felis suscipit pretium. Aenean iaculis accumsan nisl id elementum. Nulla id nibh ac nisl laoreet hendrerit efficitur non felis. Quisque leo augue, convallis ut dictum nec, convallis at ex. Aliquam et tellus posuere, sodales massa ac, dignissim est. Maecenas eu libero urna. Donec vitae purus consequat, semper nulla a, luctus dui. Pellentesque sit amet dignissim nulla, eget tristique dolor. Nunc tristique orci sagittis velit condimentum, vel feugiat ante gravida. Quisque ac orci sed odio euismod gravida.

Donec ante elit, convallis ac commodo et, dignissim at massa. Nulla lectus erat, convallis sit amet lacus nec, suscipit vehicula purus. Sed purus quam, vestibulum nec blandit id, ultrices nec augue. Cras in consequat sem. Aliquam non nunc et leo aliquam aliquet. Aliquam hendrerit turpis et sem dictum, et tincidunt odio consectetur. Quisque faucibus rhoncus augue vitae tincidunt. Etiam nulla mi, feugiat et lacus quis, consectetur dapibus eros. Aliquam eu luctus odio.

Integer pharetra eu turpis vitae sollicitudin. Maecenas vestibulum dui ante, a pulvinar purus euismod vel. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam ligula mi, posuere sit amet viverra non, ultricies id mauris. Curabitur condimentum, ligula a tincidunt condimentum, nisi justo sagittis nisi, sed tincidunt libero justo auctor nisl. Suspendisse eu tristique magna. Sed vel mauris lobortis, faucibus lorem vitae, pulvinar purus. Quisque vulputate risus quis ante ornare, quis vehicula neque eleifend. Cras volutpat, ex vel fringilla lacinia, magna nisi hendrerit sem, id aliquet nibh elit porta ante. Vestibulum tempus viverra libero sit amet porttitor. Donec eleifend euismod nisl ac tristique. Cras commodo ipsum non velit volutpat, a accumsan erat maximus. Fusce ultrices justo at odio aliquam condimentum.

Nullam quis arcu ex. Duis ultricies arcu eget lacinia venenatis. Etiam consequat iaculis augue a elementum. Nulla facilisi. Sed ultrices gravida elit vitae gravida. Donec interdum sollicitudin mauris nec mattis. Praesent lorem sapien, dictum sed sapien eget, interdum fringilla massa.

Ut et tortor vitae mauris rhoncus placerat. Etiam aliquam mattis ornare. Vivamus sed diam eget lorem aliquet ultrices vitae vel est. Duis efficitur purus massa, id pellentesque sapien sollicitudin a. Curabitur accumsan est quis commodo vestibulum. Aenean quis sem ultricies, vestibulum nisl at, accumsan felis. Donec ac tempus nisl, eget rutrum dui. Sed posuere consequat mi, blandit dapibus urna venenatis eget. Phasellus et rutrum justo.

Etiam luctus dapibus hendrerit. Proin a metus molestie, fringilla erat a, lobortis tortor. Suspendisse vitae nibh id diam maximus bibendum sed id felis. Cras quis elit eget enim aliquam vulputate. Aliquam dapibus aliquam tortor sed rutrum. Phasellus sapien nisl, congue a scelerisque vitae, auctor fringilla eros. Donec eget auctor dolor, eget tristique quam. Donec fringilla sit amet nisl vitae commodo. Morbi congue dolor leo. Pellentesque eleifend suscipit ante, vel pulvinar diam luctus et. Sed sed mi pellentesque, vestibulum odio dignissim, placerat nibh. Nam sed sollicitudin tellus. Phasellus dolor sapien, rhoncus non magna eu, tempor molestie metus. Fusce imperdiet arcu id ligula sodales tempor. Sed hendrerit est at mi rhoncus, quis varius urna viverra. Mauris pellentesque aliquam nisl, euismod euismod odio dictum et.

Sed et nisl nisl. Etiam et tempus diam. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec elementum, nulla eget dapibus egestas, risus risus malesuada dui, id congue eros nisl ultrices orci. Aliquam laoreet feugiat rutrum. Aenean laoreet, neque ullamcorper vulputate volutpat, lectus libero mollis mi, quis mattis leo sapien id nibh. Phasellus imperdiet ac metus et molestie. Aliquam malesuada ipsum vel nisi facilisis, ut viverra lectus convallis.

In ac mi et nunc ullamcorper posuere vitae in dolor. Nunc suscipit elementum nulla, vitae sodales risus ullamcorper ut. Aenean non leo cursus, maximus dui vitae, lobortis massa. Maecenas arcu tortor, facilisis nec varius ut, imperdiet in arcu. Integer gravida convallis felis non mattis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Phasellus id lorem pharetra, molestie ante vel, suscipit risus. Nullam vestibulum, sapien in commodo molestie, libero sapien sagittis eros, in placerat ipsum lectus a lectus. Vestibulum porta posuere fringilla. Fusce at eros leo. Integer id magna ac nibh malesuada lobortis non in lorem. Fusce rutrum orci ac justo efficitur pellentesque. Cras ac dolor feugiat, vulputate mi non, efficitur sem. Morbi at rutrum risus, sit amet rutrum dui. Proin sem metus, tincidunt sodales semper sit amet, eleifend mollis felis.

Nullam dignissim molestie elementum. Nam vulputate nunc vel mi varius sollicitudin. Vivamus vestibulum mollis enim, id accumsan justo cursus vitae. Etiam non nulla nec sapien ultrices ornare sit amet et arcu. In hac habitasse platea dictumst. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque dictum ipsum quis sem hendrerit, eu imperdiet mi varius. Donec sed erat ultricies, pretium leo id, accumsan quam. Etiam tempus a quam eget imperdiet. Sed justo elit, hendrerit sit amet ullamcorper a, lacinia ac orci. Sed ultrices tempor ornare. Praesent vitae est accumsan, venenatis erat congue, suscipit felis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc vel dui libero.

Phasellus hendrerit quis erat a semper. Proin sagittis magna ut libero dignissim bibendum. Maecenas aliquet sapien ut magna hendrerit, eget pharetra diam mattis. Cras finibus nibh at ante tristique vehicula. Duis nec ante lobortis, volutpat justo eu, rutrum ipsum. Quisque pellentesque velit et vehicula tincidunt. Maecenas imperdiet risus sit amet pulvinar tincidunt. In aliquam justo eget quam lacinia porta. Donec lectus metus, aliquam in faucibus ac, faucibus vitae lorem. Morbi sed tempus quam. Nulla in eleifend dui. Vivamus id ultricies nunc. In sapien nisl, mollis et massa vel, pharetra cursus justo. Sed eleifend volutpat dolor. Nullam pharetra imperdiet porttitor.

Vestibulum tortor odio, consectetur sed tempus a, ornare ut sem. Sed ut pretium erat, id tempor libero. Praesent pellentesque, lorem sit amet volutpat sagittis, dolor tellus laoreet lacus, non commodo dui neque a tortor. Nam rhoncus rutrum auctor. Morbi eu finibus velit. Morbi fringilla ex tellus. In efficitur gravida libero. Maecenas sollicitudin purus vel sem accumsan, at pretium orci maximus. Donec nec justo bibendum arcu sodales varius in non nisl. Sed consequat placerat sollicitudin. Vestibulum mollis, magna vel congue facilisis, lectus dolor faucibus quam, a lacinia purus purus in ante. Donec laoreet lacus elit, ut imperdiet libero ornare vel. Nam id hendrerit tortor. In convallis magna non dui bibendum porttitor vel nec magna. In aliquet dolor neque.

Etiam ornare justo at sollicitudin auctor. Aliquam erat volutpat. Nulla volutpat massa vel elit consectetur facilisis. Maecenas ut erat nec est tristique accumsan. Fusce suscipit justo a nulla tempor lobortis. Aliquam sem ante, laoreet ut dapibus nec, dapibus auctor nunc. Phasellus tellus dui, interdum ac leo et, aliquam molestie nisl. Pellentesque at lectus vitae libero auctor dignissim ac non erat. Aliquam mollis fringilla leo, nec varius neque bibendum et. Nunc fringilla sem enim, et varius nisl faucibus id. Nam erat sapien, egestas eu lobortis vel, semper in diam. Praesent eu magna gravida, pretium ligula condimentum, blandit ipsum. Aliquam sollicitudin eu ligula at pharetra.

Etiam dictum faucibus sollicitudin. Etiam ultrices egestas sem, id pharetra felis porttitor sed. Interdum et malesuada fames ac ante ipsum primis in faucibus. Quisque quis ex vitae nibh fermentum porta at a lorem. Nulla tellus velit, consectetur at mi at, lacinia interdum metus. Fusce lobortis sed magna et fermentum. Aliquam eu mattis orci. Fusce nec urna at risus feugiat bibendum.

Proin et nunc sed lorem mollis sollicitudin. Mauris sed quam ut dolor elementum placerat vitae ac massa. Fusce at sapien quis lacus malesuada ullamcorper. Aenean et tincidunt arcu, vitae rutrum sapien. Vestibulum at suscipit lorem. Duis aliquam vel ipsum id vestibulum. Duis sit amet dui orci. Cras et dui interdum, facilisis ante at, molestie nibh. Nullam eu tristique leo, vitae facilisis orci. Fusce malesuada nisl eu velit bibendum aliquam. Aliquam laoreet nec eros sed ultricies. Praesent nec dignissim ligula, id aliquam nisl. Morbi pellentesque pharetra tempor. Ut faucibus ex sit amet tellus ullamcorper, quis suscipit augue condimentum. Fusce at magna ac turpis bibendum viverra interdum sed nisi.

Nullam eget lorem vitae dolor placerat commodo porta at nulla. In eget tortor placerat, mattis augue at, gravida ligula. Curabitur suscipit, nulla semper commodo molestie, elit est pulvinar nibh, eget tristique felis urna vitae lacus. Morbi sodales augue non ex suscipit posuere. Aenean et ipsum risus. Fusce sit amet nulla efficitur, scelerisque justo sit amet, ullamcorper felis. Mauris cursus urna diam, sit amet pretium libero mollis vel. Aliquam quis quam a purus gravida rhoncus. Suspendisse ut nunc lacus. Ut ornare metus a iaculis ornare. Cras pretium enim sed felis cursus, id dignissim neque pretium. Suspendisse porttitor imperdiet nisl, at molestie leo congue eu. Phasellus hendrerit quis ipsum ut suscipit. Maecenas augue elit, pulvinar et tristique non, placerat id ex. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed ullamcorper placerat urna non ullamcorper.

Suspendisse placerat tellus vitae nunc suscipit, a ultrices sem vestibulum. Nunc ultrices, eros et sollicitudin pellentesque, nunc neque posuere turpis, quis mattis diam enim at diam. In id elit velit. Morbi non egestas sem, nec finibus augue. In suscipit, lacus suscipit varius finibus, dui orci sodales risus, ac congue nisl nulla mollis orci. Aliquam non ligula condimentum, pellentesque metus sed, elementum magna. Mauris laoreet vestibulum fringilla. Proin iaculis ligula sed orci suscipit, id faucibus sem viverra. Duis consequat, est nec tempus congue, quam turpis rhoncus tortor, quis pretium leo felis ut sem. Nulla in massa a metus commodo eleifend.

Nulla in ligula condimentum, pretium lacus vitae, feugiat sem. Integer eu ultricies justo. Fusce sed velit nec ante vestibulum mollis et eu lectus. Nulla quis purus vitae diam convallis ultrices eget congue velit. Morbi ultrices metus eu euismod rhoncus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Cras iaculis aliquam nisl a lacinia. Etiam at lectus nec nisl convallis faucibus. Donec sed vestibulum tellus. Vestibulum lobortis dolor magna, et euismod nunc convallis vitae. Nunc quis ligula facilisis, aliquam ex non, convallis mauris. Vivamus faucibus eu magna at varius. Nunc tempus gravida faucibus. Nulla id sapien vehicula, efficitur justo nec, tempus lectus. Integer volutpat magna in sem faucibus, non facilisis nibh faucibus. Nullam et risus vitae leo sollicitudin porttitor non sed sem.

Aenean tempor egestas dapibus. Sed sit amet urna vel purus egestas placerat a vitae tortor. Cras a leo nisl. Quisque sem ipsum, tristique at ligula suscipit, maximus vestibulum leo. Proin pretium massa a efficitur convallis. Sed nec urna dictum, mattis lectus in, dapibus metus. Phasellus egestas at nibh placerat vulputate. Maecenas dolor libero, aliquet sit amet ipsum vel, fermentum varius metus. Cras mattis condimentum mauris id pulvinar. Aenean ut nisi accumsan, fringilla mi a, ultricies diam.

Proin pellentesque nisi blandit magna vehicula, ut consectetur orci porttitor. Praesent porta magna sed ipsum euismod, eget tempor lacus cursus. Quisque sit amet metus tellus. Duis elit eros, suscipit id facilisis vitae, imperdiet quis augue. Vestibulum ac libero at libero porta lobortis ac porta libero. Proin a maximus sapien. Curabitur vel dui non justo aliquet lacinia at eu lorem. Cras ornare molestie congue. Mauris id malesuada diam, at laoreet risus. Proin hendrerit ut quam vitae dignissim. Aliquam libero nunc, laoreet finibus auctor at, ultricies eget risus. Nulla ac diam in risus fermentum lobortis. Curabitur imperdiet ultricies sagittis.

Aliquam vitae viverra libero. Maecenas dolor purus, euismod sed tortor eu, congue vestibulum massa. Fusce interdum finibus malesuada. Mauris scelerisque, turpis eu iaculis facilisis, nibh neque elementum nisi, a hendrerit elit mi viverra augue. Ut ut scelerisque lectus. Mauris nec purus neque. Maecenas vulputate in velit quis lobortis. Integer nec nulla vulputate tortor fringilla aliquam sit amet nec velit. Integer quis risus id augue mattis finibus. Morbi odio nisl, consequat ac ante eu, pulvinar gravida nisl. Donec a pharetra ipsum. Etiam convallis erat sed viverra convallis. Sed sagittis sagittis ultricies. Phasellus vel tempor eros. Praesent auctor nisl a elit pretium lobortis.

Etiam enim ante, euismod sit amet sollicitudin quis, imperdiet vitae eros. Nunc tempus diam massa, vitae feugiat lectus mollis eget. Sed molestie odio at felis lacinia vestibulum. Curabitur quam augue, tempus quis finibus et, imperdiet eu ligula. Vestibulum finibus nunc sit amet volutpat varius. Fusce non vehicula nisi. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nam non venenatis leo.

Curabitur maximus vulputate porttitor. Etiam aliquet viverra nisi, ac feugiat orci tristique id. In fringilla odio vitae nunc ornare tempus. Nulla tempor eu urna fringilla tincidunt. Pellentesque imperdiet lectus nec tincidunt convallis. Vivamus feugiat augue eget leo mattis convallis. Duis id arcu leo. Donec eu placerat ligula, a porttitor quam. Sed scelerisque eleifend turpis, id sodales sem volutpat id. Aenean posuere dui ut gravida consectetur. Donec bibendum sagittis lectus, sit amet faucibus mauris mattis a. Integer tincidunt tristique mauris, vitae consectetur dui sollicitudin eu.

Sed mollis a erat nec sagittis. Proin id dignissim ligula. In tempus malesuada augue. Morbi tellus nibh, scelerisque quis auctor ut, faucibus sagittis tortor. Suspendisse ut semper nunc, in placerat nibh. Nulla non augue velit. Nulla ipsum leo, vestibulum quis ipsum vitae, ullamcorper eleifend purus. Vivamus euismod lobortis diam eget posuere. Sed at enim erat. Nam imperdiet porta tortor, ut pretium metus efficitur a. Sed eget hendrerit sapien. Aenean et cursus purus.

Sed efficitur ut augue ut placerat. Donec bibendum, felis ac lacinia tristique, magna libero vulputate elit, ac luctus tortor libero et ligula. Donec gravida eget arcu quis laoreet. Pellentesque ornare dolor nec tellus aliquet tincidunt. Donec hendrerit eros sed elit tempor porttitor vel eget dolor. Phasellus faucibus sem non nunc interdum, sed aliquam neque faucibus. Suspendisse potenti. Pellentesque eget fermentum felis. Pellentesque blandit vel arcu sed vulputate. Integer imperdiet diam non neque rutrum, sit amet elementum nibh luctus. Praesent at orci aliquam.

Aenean eget fringilla eros. Quisque ut mi mi. Duis imperdiet, magna quis malesuada vehicula, dui urna dapibus sapien, vitae mollis augue erat ac lectus. Donec ultrices metus et felis suscipit pretium. Aenean iaculis accumsan nisl id elementum. Nulla id nibh ac nisl laoreet hendrerit efficitur non felis. Quisque leo augue, convallis ut dictum nec, convallis at ex. Aliquam et tellus posuere, sodales massa ac, dignissim est. Maecenas eu libero urna. Donec vitae purus consequat, semper nulla a, luctus dui. Pellentesque sit amet dignissim nulla, eget tristique dolor. Nunc tristique orci sagittis velit condimentum, vel feugiat ante gravida. Quisque ac orci sed odio euismod gravida.

Donec ante elit, convallis ac commodo et, dignissim at massa. Nulla lectus erat, convallis sit amet lacus nec, suscipit vehicula purus. Sed purus quam, vestibulum nec blandit id, ultrices nec augue. Cras in consequat sem. Aliquam non nunc et leo aliquam aliquet. Aliquam hendrerit turpis et sem dictum, et tincidunt odio consectetur. Quisque faucibus rhoncus augue vitae tincidunt. Etiam nulla mi, feugiat et lacus quis, consectetur dapibus eros. Aliquam eu luctus odio.

Sed a maximus nisi, sed malesuada nisi. Donec vitae mattis turpis, eget fringilla dui. Mauris sagittis, nibh eget lacinia ultricies, nulla quam lobortis diam, eget volutpat tortor urna a ex. Donec vestibulum dictum nunc a convallis. In tincidunt ornare fringilla. Nulla ac ante sit amet nulla suscipit pretium. Pellentesque odio lectus, consequat sed nisi ac, vulputate gravida elit. Sed in metus at erat laoreet pretium ut at est.

Donec lectus turpis, eleifend eu molestie id, porttitor sed ipsum. In hac habitasse platea dictumst. Aenean feugiat lorem ut tortor fermentum, in vulputate nisi posuere. Curabitur bibendum elementum eros sit amet sollicitudin. Mauris venenatis, leo id luctus viverra, odio nibh fermentum nibh, nec vulputate quam nulla non eros. Nunc ullamcorper vehicula lectus, eu ultricies velit consequat vel. Phasellus vel dui consectetur odio faucibus bibendum. Suspendisse sodales fermentum est, at dignissim justo fringilla et. Nulla porttitor lectus et eros viverra, accumsan consectetur est pellentesque. Sed sollicitudin urna diam, vel luctus lacus tristique ac. Donec a porta sem, ac tempus nibh. In a odio tincidunt lacus porta mattis. Aliquam venenatis nisi at justo ornare fermentum. Nulla sit amet tortor ligula. Sed sed eros arcu. Pellentesque venenatis ante in consequat scelerisque.

Mauris dapibus varius nisl, quis blandit neque venenatis in. Phasellus aliquam, arcu in rhoncus blandit, augue arcu placerat nunc, a laoreet ipsum tortor eget magna. In varius, nisl eu molestie accumsan, lorem est porttitor eros, in bibendum risus quam nec leo. Quisque dapibus porttitor velit euismod tempor. Morbi maximus sapien quis arcu ullamcorper ornare. In eu velit quis massa vulputate convallis id semper felis. Morbi scelerisque enim eros, sit amet luctus risus molestie sit amet. Ut accumsan mollis massa, vitae mattis tellus volutpat sed.

Aenean et odio tempor, auctor mauris sit amet, facilisis leo. In finibus non enim eu malesuada. Integer suscipit nibh id finibus bibendum. Integer vehicula, nunc nec tristique rhoncus, ligula lectus posuere velit, eu tempus eros elit non leo. Morbi ut tristique dui, id sollicitudin risus. Curabitur sit amet elit varius erat porttitor volutpat sed nullam.

Maecenas nisi metus, malesuada ut imperdiet a, bibendum eu felis. Praesent sed magna ante. Aenean quis lacus et nisi malesuada luctus. Curabitur efficitur eros eget neque iaculis, eu porttitor ex pretium. Mauris nec massa ac sapien varius dapibus sed eu libero. Suspendisse bibendum a urna ut interdum. Vestibulum id lacus semper, semper ante vel, maximus tellus. Nam metus quam, pellentesque vel sodales quis, lobortis nec mi. Etiam eget purus nulla. Phasellus faucibus tellus in pellentesque blandit. Nam tincidunt sem non commodo tincidunt. Nulla euismod auctor enim, sit amet pretium elit scelerisque convallis. Vivamus rhoncus, elit non ullamcorper congue, mi felis elementum ligula, vel sagittis nunc neque et ipsum.

Integer vulputate tempus mollis. Cras ultricies feugiat odio, in fermentum velit interdum sit amet. Vestibulum condimentum varius metus, at aliquam lectus rhoncus vitae. Curabitur sodales varius libero eu porta. Fusce mattis sapien eu ipsum molestie, eu volutpat arcu faucibus. Fusce fermentum tempor sapien quis pellentesque. Duis ornare lacus eget lorem lobortis viverra. Curabitur luctus interdum laoreet. Morbi laoreet ut metus finibus tempor. Curabitur feugiat tristique augue ut faucibus. Cras varius ipsum nisi, et pellentesque nibh elementum at. In laoreet, lectus non semper dictum, nisi erat eleifend nisi, vitae gravida tellus sem sit amet urna. Ut odio tellus, imperdiet ut blandit a, laoreet finibus lectus. Donec tristique, eros pellentesque faucibus pulvinar, dui turpis maximus justo, vitae luctus velit risus in diam. Sed id vehicula nulla, vel efficitur elit.`

