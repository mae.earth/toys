/* mae.earth/toys/silo/service.go
 * mae 12017
 */
package main

import (
	"log"
	"os"
	"fmt"
	"time"
	"strings"
	"flag"
	"bytes"
	"net/http"
	"os/signal"
	"syscall"

	"github.com/boltdb/bolt"
	"github.com/gorilla/mux"
)

const (
	MaxDepth int = 10
)

var internal struct {
	DB *bolt.DB
}

func construct(b *bolt.Bucket,depth int) error {
	if depth >= MaxDepth {
		return nil
	}

	/* check for len(buckets) in this bucket */
	if b == nil {
		return nil
	}

	c := b.Cursor()

	keys := 0
	buckets := 0

	for k, v := c.First(); k != nil; k, v = c.Next() {
		if v == nil { /* then bucket */

			b1 := b.Bucket(k)
			if err := construct(b1,depth + 1); err != nil {
				return fmt.Errorf("construct error of depth %d + 1 -- %v",depth,err)
			}
			
			buckets ++

		} else {

			keys ++

		}
	}

	if keys == 0 {
		for i := 0; i < Number(20) + 1; i++ {
	
				/* add some content */
				_,body := ipsomArticle()
				
				sleeve := NewSleeve("text/plain","text",body)
				content,err := sleeve.Encode()
				if err != nil {
					return err
				}
	
				if err := b.Put([]byte(uuid()),content); err != nil {
					return fmt.Errorf("error putting -- %v",err)
				}
			}
		}

	if buckets == 0 {
		for i := 0; i < Number(10); i++ {

			if _,err := b.CreateBucketIfNotExists([]byte(uuid())); err != nil {

				return fmt.Errorf("error creating bucket -- %v",err)
			}
		}
	}

	return nil
}


func main() {

	build := flag.Bool("build",false,"build random content")
	root := flag.Bool("root",false,"build root buckets")

	flag.Parse()

	

	/* load bolt database */
	db,err := bolt.Open("out.db",0600,&bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		fmt.Fprintf(os.Stderr,"error opening database out.db -- %v\n",err)
		os.Exit(1)
	}
	defer db.Close()

	internal.DB = db



	if *build {

		fmt.Printf("building silo...\n")	
			

		/* create a database */	
		err = db.Update(func(tx *bolt.Tx) error {
			
			if *root {
				for i := 0; i < 5; i++ {
					 tx.CreateBucket([]byte(uuid()))
				}	
			}
					
			err := tx.ForEach(func(k []byte,b *bolt.Bucket) error {
	
				/* go through and add some more buckets */
				return construct(b,0)							
			})
	
		 	if err != nil {
				return err
			}
	
			return nil
		})
		
		if err != nil {
			fmt.Fprintf(os.Stderr,"error creating database -- %v\n",err)
			os.Exit(1)
		}

		fmt.Printf("\rbuilding silo...done\n")
	}

	/* run service here */
	fmt.Printf("running silo...\n")

	sigs := make(chan os.Signal,1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		sig := <- sigs
		fmt.Printf("signal %v recieved...\nEnd of Line.\n",sig)
		os.Exit(1)
	}()

	r := mux.NewRouter()

	r.HandleFunc("/",Handler)
	r.PathPrefix("/").Subrouter().NotFoundHandler = &Serve{}

	http.Handle("/",r)
	fmt.Printf("interface running on http://localhost:8080\n")
	if err := http.ListenAndServe(":8080",nil); err != nil {
		fmt.Fprintf(os.Stderr,"error running http service -- %v\n",err)
		os.Exit(1)
	}
}

type Serve struct {

}

func (s *Serve) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	Handler(w,req)
}

const docheader = `<!DOCTYPE html>
<html land="en">
<head>
	<title>silo</title>
</head>
<body>`

const docfooter = `</body></html>`



func Handler(w http.ResponseWriter,req *http.Request) {

	log.Printf("handler [%s]\n",req.URL.Path)

	root := req.URL.Path
	if root != "/" {
		if root[0] == '/' {
			root = root[1:]
		}
	} else {
		root = ""
	}

	content,err := View(internal.DB,root,Options{}) /* Add more options */
	if err != nil {
		http.Error(w,err.Error(),500)
		return
	}


	if dir,ok := content.(*Directory); ok {

		/* RENDER DIRECTORY */
		buf := bytes.NewBuffer(nil)	
		buf.Write([]byte(docheader))

		if len(dir.Buckets) > 0 {
	
			buf.Write([]byte("<dir>\n\t<ul>\n"))

			for _, b := range dir.Buckets {
				buf.Write([]byte(fmt.Sprintf(`<li><a href="%s">%s</a></li>`,b.Url,b.Name)))
			}

			buf.Write([]byte("</ul></dir>\n\n"))
		}

		if len(dir.Items) > 0 {
			buf.Write([]byte("<ul>\n"))
		
			for _, f := range dir.Items {
				buf.Write([]byte(fmt.Sprintf(`<li><a href="%s">%s........................%dbytes</li>`,f.Url,f.Name,f.Length)))
			}

			buf.Write([]byte("</ul>\n"))
		}	

		buf.Write([]byte(docfooter))

		w.Write(buf.Bytes())
		return
	}
	
	file,ok := content.(*Sleeve)
	if !ok {
		http.Error(w,"invalid",500)
		return
	}

	/* RENDER FILE */
	w.Header().Set("Content-Type",file.ContentType)
	w.Write(file.Content)
}

type File struct {
	Name string
	Url string
	Length int
}

type Dir struct {
	Name string
	Url string
}

type Directory struct {
	Name string
	Buckets []Dir
	Items []File
}

type Options struct {

}

/* View */
func View(db *bolt.DB,root string,options Options) (interface{},error) {

	log.Printf("View [%s[ {%+v}\n",root,strings.Split(root,"/"))


	var out interface{}

	err := db.View(func(tx *bolt.Tx) error {

		var b *bolt.Bucket
		var pb *bolt.Bucket

		if root == "" {

			dir := &Directory{Name: "root"}
			dir.Buckets = make([]Dir,0)
			dir.Items = make([]File,0)

			c := tx.Cursor()

			for k, v := c.First(); k != nil; k, v = c.Next() {
				if v == nil { /* then bucket */
				
					dir.Buckets = append(dir.Buckets,Dir{Name:string(k)})

				} else {

					dir.Items = append(dir.Items,File{Name: string(k), Length: len(v)})
				}
			}	

			out = dir
			return nil
		} /* === */

		parts := strings.Split(root,"/")

		for _,part := range parts {
			pb = b
			if b == nil {
				b = tx.Bucket([]byte(part))
				if b == nil {
					break
				}
			} else {
				b = b.Bucket([]byte(part))
				if b == nil {
					break
				}
			}
		}

		if b == nil { /* then check for item */
			if pb == nil {
				return bolt.ErrBucketNotFound
			}

			name := parts[ len(parts) - 1 ]

			content := pb.Get([]byte(name))
			if content == nil {
				return bolt.ErrBucketNotFound
			}			

			sleeve,err := ToSleeve(content)
			if err != nil {
				return err
			}

			out = sleeve 
			return nil
		} 

		/* then list the bucket as a directory */	
		name := parts[ len(parts) - 1 ]

		dir := &Directory{Name: name}
		dir.Buckets = make([]Dir,0)
		dir.Items = make([]File,0)

		c := b.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			if v == nil { /* then bucket */
				
				dir.Buckets = append(dir.Buckets,Dir{Name:string(k)})

			} else {

				dir.Items = append(dir.Items,File{Name: string(k), Length: len(v)})
			}
		}

		out = dir
		
		return nil
	})

	return out,err
}
	




