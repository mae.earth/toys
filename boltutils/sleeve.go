/* mae.earth/toys/boltutils/sleeve.go
 * mae 12017
 */
package boltutils

import (
	"bytes"
	"encoding/binary"
	"errors"
	"time"
	"strings"

	"github.com/minio/blake2b-simd"    /* generally faster than sha1 and also has a size configuration */
	"github.com/vmihailenco/msgpack"
)

const (
	Magic   = byte('S')
	Version = byte(0)

	ChecksumLength int = 8
	TagDelimiter string = ";"
)

var (
	ErrBadMagic       = errors.New("bad magic")
	ErrBadVersion     = errors.New("bad version")
	ErrBadChecksum    = errors.New("bad checksum")
	ErrBadContentType = errors.New("bad content type")
)

/* Sleeve */
type Sleeve struct {
	Created  time.Time `msgpack:"c"` /* creation date */
	Modified time.Time `msgpack:"m"` /* last modification of the content */

	ContentType        string `msgpack:"-"`            /* MIME content type of the content */
	ContentDisposition string `msgpack:"cd,omitempty"` /* Presentatation of the content */

	Version  int    `msgpack:"v"` /* Version of this content */
	Checksum string `msgpack:"-"` /* Checksum of the content snip(sha1(content)) */

	Tags map[string]string `msgpack:"T,omitempty"` /* tags per sleeve, these can be user defined */

	Content []byte `msgpack:"-"` /* actual user content */
}

/* SetTag */
func (sleeve *Sleeve) SetTag(tag string,values ...string) *Sleeve {
	if len(tag) == 0 {
		return sleeve
	}

	if sleeve.Tags == nil {
		sleeve.Tags = make(map[string]string,0)
	}

	sleeve.Tags[ tag ] = strings.Join(values,TagDelimiter)

	return sleeve
}

/* Tag */
func (sleeve *Sleeve) Tag(tag string) []string {
	if len(tag) == 0 || sleeve.Tags == nil {
		return nil
	}

	if v,ok := sleeve.Tags[ tag ]; ok {
		return strings.Split(v,TagDelimiter)
	}

	return nil
}

/* RemoveTag */
func (sleeve *Sleeve) RemoveTag(tag string) *Sleeve {
	if len(tag) == 0 || sleeve.Tags == nil {
		return sleeve
	}

	if _,ok := sleeve.Tags[ tag ]; ok {
		if len(sleeve.Tags) == 1 {
			sleeve.Tags = nil 
		} else {
			delete(sleeve.Tags,tag)
		}
	}

	return sleeve
}

/* AppendTag */
func (sleeve *Sleeve) AppendTag(tag string,values ...string) *Sleeve {
	if len(tag) == 0 || len(values) == 0 || sleeve.Tags == nil {
		return nil
	}

	if v, ok := sleeve.Tags[ tag ]; ok {

				

		sleeve.Tags[ tag ] = v + TagDelimiter + strings.Join(values,TagDelimiter)
	}

	return sleeve
}
	
		


/* Encode */
func (sleeve *Sleeve) Encode() ([]byte, error) {

	/* For simple content lookups, rather than having to decode the entire object
		 * just to get content, we place the content and the content type at the end
	   * of the data and provide an integer at the beginning to skip the encoded
	   * sleeve data. This allows us to get the content-type and actual content
	   * without having to decode the sleeve.
	   *
	   * The content, the first N (as defined by ChecksumLength) bytes are the
	   * checksum. This allows us to check the content without having to decode
	   * the sleeve.
	*/

	msgcontent, err := msgpack.Marshal(sleeve)
	if err != nil {
		return nil, err
	}

	l := uint64(len(msgcontent))

	buf := bytes.NewBuffer(nil)
	buf.WriteByte(Magic)
	buf.WriteByte(Version)

	if err := binary.Write(buf, binary.LittleEndian, l); err != nil {
		return nil, err
	}

	buf.Write(msgcontent)

	buf.WriteByte(byte(len(sleeve.ContentType)))
	buf.Write([]byte(sleeve.ContentType))

	buf.Write([]byte(sleeve.Checksum))
	buf.Write(sleeve.Content)

	return buf.Bytes(), nil
}

/* ToSleeve */
func ToSleeve(content []byte) (*Sleeve, error) {

	var l uint64
	buf := bytes.NewBuffer(content)
	magic, err := buf.ReadByte()
	if err != nil {
		return nil, ErrBadMagic
	}

	if magic != Magic {
		return nil, ErrBadMagic
	}

	version, err := buf.ReadByte()
	if err != nil {
		return nil, ErrBadVersion
	}

	if int(version) > int(Version) {
		return nil, ErrBadVersion
	}

	if err := binary.Read(buf, binary.LittleEndian, &l); err != nil {
		return nil, err
	}

	var sleeve *Sleeve
	if err := msgpack.Unmarshal(buf.Next(int(l)), &sleeve); err != nil {
		return nil, err
	}

	ctl, err := buf.ReadByte()
	if err != nil {
		return nil, ErrBadContentType
	}

	ct := buf.Next(int(ctl))
	sleeve.ContentType = string(ct)

	ck := buf.Next(ChecksumLength)
	if len(ck) != ChecksumLength {
		return nil, ErrBadChecksum
	}

	sleeve.Checksum = string(ck)

	con := buf.Bytes()
	if len(con) > 0 {
		sleeve.Content = make([]byte, len(con))
		copy(sleeve.Content, con)

		/* do a checksum check */
		b, _ := blake2b.New(&blake2b.Config{Size: uint8(ChecksumLength)})
		b.Write(sleeve.Content)
		if string(b.Sum(nil)) != string(ck) {
			return sleeve, ErrBadChecksum
		}
	}

	return sleeve, nil
}

/* GetContent returns ContentType and Content */
func GetContent(content []byte) (string, []byte, error) {

	var l uint64
	buf := bytes.NewBuffer(content)
	magic, err := buf.ReadByte()
	if err != nil {
		return "", nil, ErrBadMagic
	}

	if magic != Magic {
		return "", nil, ErrBadMagic
	}

	version, err := buf.ReadByte()
	if err != nil {
		return "", nil, ErrBadVersion
	}

	if int(version) > int(Version) {
		return "", nil, ErrBadVersion
	}

	if err := binary.Read(buf, binary.LittleEndian, &l); err != nil {
		return "", nil, err
	}

	/* seek forward, skipping the encoded sleeve */
	buf.Next(int(l))

	ctl, err := buf.ReadByte()
	if err != nil {
		return "", nil, ErrBadContentType
	}

	ct := buf.Next(int(ctl))

	ck := buf.Next(ChecksumLength)
	if len(ck) != ChecksumLength {
		return "", nil, ErrBadChecksum
	}

	con := buf.Bytes()

	b, _ := blake2b.New(&blake2b.Config{Size: uint8(ChecksumLength)})
	b.Write(con)

	if string(ck) != string(b.Sum(nil)) {
		return string(ct), con, ErrBadChecksum
	}

	return string(ct), con, nil
}

/* GetContentWithoutChecksum */
func GetContentWithoutChecksum(content []byte) (string, []byte, error) {

	var l uint64
	buf := bytes.NewBuffer(content)
	magic, err := buf.ReadByte()
	if err != nil {
		return "", nil, ErrBadMagic
	}

	if magic != Magic {
		return "", nil, ErrBadMagic
	}

	version, err := buf.ReadByte()
	if err != nil {
		return "", nil, ErrBadVersion
	}

	if int(version) > int(Version) {
		return "", nil, ErrBadVersion
	}

	if err := binary.Read(buf, binary.LittleEndian, &l); err != nil {
		return "", nil, err
	}

	/* seek forward, skipping the encoded sleeve */
	buf.Next(int(l))

	ctl, err := buf.ReadByte()
	if err != nil {
		return "", nil, ErrBadContentType
	}

	ct := buf.Next(int(ctl))

	buf.Next(ChecksumLength)

	return string(ct), buf.Bytes(), nil
}

/* Copy */
func (sleeve *Sleeve) Copy() *Sleeve {

	nsleeve := NewSleeve(sleeve.ContentType, sleeve.ContentDisposition, sleeve.Content)
	nsleeve.Version = sleeve.Version + 1
	return nsleeve
}

/* CopyWithContent */
func (sleeve *Sleeve) CopyWithContent(content []byte) *Sleeve {

	nsleeve := NewSleeve(sleeve.ContentType, sleeve.ContentDisposition, content)
	nsleeve.Created = sleeve.Created
	nsleeve.Version = sleeve.Version + 1

	return nsleeve
}

/* NewSleeve */
func NewSleeve(ctype, cdispo string, content []byte) *Sleeve {

	sleeve := &Sleeve{Created: time.Now(), Modified: time.Now(), Version: 0, ContentType: ctype, ContentDisposition: cdispo}

	if len(content) > 0 {
		sleeve.Content = make([]byte, len(content))
		copy(sleeve.Content, content)
	}

	b, _ := blake2b.New(&blake2b.Config{Size: uint8(ChecksumLength)})
	b.Write(content)
	sleeve.Checksum = string(b.Sum(nil))

	return sleeve
}
