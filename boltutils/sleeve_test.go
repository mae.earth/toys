/* mae.earth/toys/boltutils/sleeve_test.go
 * mae 12017
 */
package boltutils

import (
	. "github.com/smartystreets/goconvey/convey"
	"testing"
	"time"

	"crypto/sha1"
	"github.com/minio/blake2b-simd"
)

func Test_Sleeve(t *testing.T) {

	Convey("Sleeve", t, func() {

		Convey("basic usage", func() {

			now := time.Now()

			sleeve := NewSleeve("text/plain", "text", []byte("helo"))
			So(sleeve, ShouldNotBeNil)
			So(sleeve.ContentType, ShouldEqual, "text/plain")
			So(sleeve.ContentDisposition, ShouldEqual, "text")
			So(string(sleeve.Content), ShouldEqual, "helo")
			So(sleeve.Version, ShouldEqual, 0)
			So(now.Before(sleeve.Created), ShouldBeTrue)
			So(now.Before(sleeve.Modified), ShouldBeTrue)

			Convey("tags",func() {

				sleeve.SetTag("foo","bar")
				So(len(sleeve.Tags),ShouldEqual,1)
				So(len(sleeve.Tag("foo")),ShouldEqual,1)
				
				sleeve.RemoveTag("foo")
				So(sleeve.Tags,ShouldBeNil)

				sleeve.AppendTag("foo","soo")
				So(sleeve.Tags,ShouldBeNil)

				sleeve.SetTag("foo","one","two","three")
				So(len(sleeve.Tags),ShouldEqual,1)
				values := sleeve.Tag("foo")
				So(len(values),ShouldEqual,3)
				So(values[0],ShouldEqual,"one")
				So(values[1],ShouldEqual,"two")
				So(values[2],ShouldEqual,"three")

				sleeve.AppendTag("foo","four","five")
				So(len(sleeve.Tags),ShouldEqual,1)
				values = sleeve.Tag("foo")
				So(len(values),ShouldEqual,5)
				So(values[3],ShouldEqual,"four")
				So(values[4],ShouldEqual,"five")

			})
				

			Convey("encode", func() {

				content, err := sleeve.Encode()
				So(err, ShouldBeNil)
				So(content, ShouldNotBeEmpty)
				So(len(content), ShouldEqual, 69)

				Convey("get content", func() {

					ct, con, err := GetContent(content)
					So(err, ShouldBeNil)
					So(ct, ShouldEqual, "text/plain")
					So(con, ShouldNotBeEmpty)
					So(string(con), ShouldEqual, "helo")
				})

				Convey("decode", func() {

					sleeve1, err := ToSleeve(content)
					So(err, ShouldBeNil)
					So(sleeve1, ShouldNotBeNil)
					So(sleeve1, ShouldNotBeNil)
					So(sleeve1.ContentType, ShouldEqual, "text/plain")
					So(sleeve1.ContentDisposition, ShouldEqual, "text")
					So(string(sleeve1.Content), ShouldEqual, "helo")
					So(sleeve1.Version, ShouldEqual, 0)
					So(now.Before(sleeve1.Created), ShouldBeTrue)
					So(now.Before(sleeve1.Modified), ShouldBeTrue)
				})
			})
		})
	})
}

func Benchmark_Sha1(b *testing.B) {

	for i := 0; i < b.N; i++ {

		sha := sha1.New()
		sha.Write([]byte(`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non convallis dolor, eu vehicula magna. Vivamus dapibus placerat arcu at dapibus. Aenean metus quam, eleifend vestibulum accumsan et, tristique non orci. Vivamus erat libero, eleifend vel condimentum et, pretium sit amet justo. Maecenas in pharetra ante, in suscipit sapien. Quisque interdum, ante vel aliquam congue, nibh felis lobortis dui, non convallis purus neque vitae felis. Curabitur imperdiet porta leo, at bibendum arcu pellentesque sed. Duis eu suscipit metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vehicula venenatis ligula, eu blandit ante iaculis et. In ultrices neque faucibus imperdiet porta. Nam venenatis est magna, id aliquam leo eleifend in.`))
		sha.Sum(nil)
	}
}

func Benchmark_Blake2b(b *testing.B) {

	for i := 0; i < b.N; i++ {

		blake, _ := blake2b.New(&blake2b.Config{Size: 8})
		blake.Write([]byte(`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non convallis dolor, eu vehicula magna. Vivamus dapibus placerat arcu at dapibus. Aenean metus quam, eleifend vestibulum accumsan et, tristique non orci. Vivamus erat libero, eleifend vel condimentum et, pretium sit amet justo. Maecenas in pharetra ante, in suscipit sapien. Quisque interdum, ante vel aliquam congue, nibh felis lobortis dui, non convallis purus neque vitae felis. Curabitur imperdiet porta leo, at bibendum arcu pellentesque sed. Duis eu suscipit metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vehicula venenatis ligula, eu blandit ante iaculis et. In ultrices neque faucibus imperdiet porta. Nam venenatis est magna, id aliquam leo eleifend in.`))
		blake.Sum(nil)
	}
}

func Benchmark_SleeveEncode(b *testing.B) {

	b.StopTimer()

	sleeve := NewSleeve("text/plain", "text", []byte(`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non convallis dolor, eu vehicula magna. Vivamus dapibus placerat arcu at dapibus. Aenean metus quam, eleifend vestibulum accumsan et, tristique non orci. Vivamus erat libero, eleifend vel condimentum et, pretium sit amet justo. Maecenas in pharetra ante, in suscipit sapien. Quisque interdum, ante vel aliquam congue, nibh felis lobortis dui, non convallis purus neque vitae felis. Curabitur imperdiet porta leo, at bibendum arcu pellentesque sed. Duis eu suscipit metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vehicula venenatis ligula, eu blandit ante iaculis et. In ultrices neque faucibus imperdiet porta. Nam venenatis est magna, id aliquam leo eleifend in.`))

	b.StartTimer()

	for i := 0; i < b.N; i++ {

		sleeve.Encode()
	}
}

func Benchmark_SleeveGetContent(b *testing.B) {

	b.StopTimer()

	sleeve := NewSleeve("text/plain", "text", []byte(`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non convallis dolor, eu vehicula magna. Vivamus dapibus placerat arcu at dapibus. Aenean metus quam, eleifend vestibulum accumsan et, tristique non orci. Vivamus erat libero, eleifend vel condimentum et, pretium sit amet justo. Maecenas in pharetra ante, in suscipit sapien. Quisque interdum, ante vel aliquam congue, nibh felis lobortis dui, non convallis purus neque vitae felis. Curabitur imperdiet porta leo, at bibendum arcu pellentesque sed. Duis eu suscipit metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vehicula venenatis ligula, eu blandit ante iaculis et. In ultrices neque faucibus imperdiet porta. Nam venenatis est magna, id aliquam leo eleifend in.`))

	content, err := sleeve.Encode()
	if err != nil {
		b.Fatal(err)
	}

	b.StartTimer()

	for i := 0; i < b.N; i++ {

		GetContent(content)
	}
}

func Benchmark_SleeveGetContentWithoutChecksum(b *testing.B) {

	b.StopTimer()

	sleeve := NewSleeve("text/plain", "text", []byte(`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non convallis dolor, eu vehicula magna. Vivamus dapibus placerat arcu at dapibus. Aenean metus quam, eleifend vestibulum accumsan et, tristique non orci. Vivamus erat libero, eleifend vel condimentum et, pretium sit amet justo. Maecenas in pharetra ante, in suscipit sapien. Quisque interdum, ante vel aliquam congue, nibh felis lobortis dui, non convallis purus neque vitae felis. Curabitur imperdiet porta leo, at bibendum arcu pellentesque sed. Duis eu suscipit metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vehicula venenatis ligula, eu blandit ante iaculis et. In ultrices neque faucibus imperdiet porta. Nam venenatis est magna, id aliquam leo eleifend in.`))

	content, err := sleeve.Encode()
	if err != nil {
		b.Fatal(err)
	}

	b.StartTimer()

	for i := 0; i < b.N; i++ {

		GetContentWithoutChecksum(content)
	}
}

func Benchmark_SleeveDecode(b *testing.B) {

	b.StopTimer()

	sleeve := NewSleeve("text/plain", "text", []byte(`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non convallis dolor, eu vehicula magna. Vivamus dapibus placerat arcu at dapibus. Aenean metus quam, eleifend vestibulum accumsan et, tristique non orci. Vivamus erat libero, eleifend vel condimentum et, pretium sit amet justo. Maecenas in pharetra ante, in suscipit sapien. Quisque interdum, ante vel aliquam congue, nibh felis lobortis dui, non convallis purus neque vitae felis. Curabitur imperdiet porta leo, at bibendum arcu pellentesque sed. Duis eu suscipit metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vehicula venenatis ligula, eu blandit ante iaculis et. In ultrices neque faucibus imperdiet porta. Nam venenatis est magna, id aliquam leo eleifend in.`))

	content, err := sleeve.Encode()
	if err != nil {
		b.Fatal(err)
	}

	b.StartTimer()

	for i := 0; i < b.N; i++ {

		ToSleeve(content)
	}
}
